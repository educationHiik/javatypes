/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatypes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author vaganovdv
 */
public class PointList
{
    // Список точек (база данных точек)
    private List<Point> points = new ArrayList<>();

    public PointList()
    {
        
    }
        
       /**
     * Метод поиска точки по 3-координатам
     *
     * @return
     */
    public Optional<List<Point>> findPoint(Point pointToFind)
    {
        // 
        List<Point> foundPoints = new ArrayList<>();
        /*
        System.out.println("Поиск точки с координатами "
                + "X = [" + pointToFind.getX() + "] "
                + "Y = [" + pointToFind.getY() + "] "
                + "Z = [" + pointToFind.getZ() + "] ");
         */
        // Зануляем opt и указываем на "пустоту" - нет результата выполнения функции
        Optional<List<Point>> opt = Optional.empty();
        int k = points.size();
        //System.out.println("Размер списка {" + k + "}");

        for (int счетчик = 0; счетчик < k; счетчик++)
        {

            double X = points.get(счетчик).getX();
            double Y = points.get(счетчик).getY();
            double Z = points.get(счетчик).getZ();
            /*
            System.out.println("Точка ["+счетчик+"] "
                + points.get(счетчик).getName()+" "
                + "X = ["+X+"] "
                + "Y = ["+Y+"] "
                + "Z = ["+Z+"] ");
             */
            //                          союз И (логическая функции)
            //                             |
            if (pointToFind.getX() == X && pointToFind.getY() == Y && pointToFind.getX() == X)
            {
               /*
                System.out.println("Точка найдена " + points.get(счетчик).getName() + " "
                        + "X = [" + X + "] "
                        + "Y = [" + Y + "] "
                        + "Z = [" + Z + "] ");
                */
                foundPoints.add(points.get(счетчик));
                // Размещаем спискок найденных точек в перемнной opt
                opt = Optional.of(foundPoints);

            }
        }

       // System.out.println("Поиск завершен, найдено   [" + foundPoints.size() + "] точек, соответствующих критерию поиска ");

        return opt;
    }
    
    
    
    
    /**
     * @return the points
     */
    public List<Point> getPoints()
    {
        //int  k = points.size();
        //System.out.println("Размер списка {"+k+"}");
        return points;
    }

    /**
     * @param points the points to set
     */
    public void setPoints(List<Point> points)
    {
        this.points = points;
    }

}

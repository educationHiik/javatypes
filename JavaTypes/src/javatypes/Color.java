/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatypes;

/**
 *
 * @author vaganovdv
 */
public class Color
{

    // Внутренные поля класса Color
    private int red = 1;
    private int green = 3;
    private int blue = 4;
    
    public Color ( int r, int g, int blue)
    {
       red = r;
       green = g;
       this.blue = blue;
    }        
    
    /**
     * 
     * @param color - цвет объекта
     */
    public Color(String  color)
    {
        // if (color = "красный")
        if (  color.equalsIgnoreCase("красный"))
        {
           red = 255;
           green = 0;
           blue  = 0;
        }    
        
        if (  color.equalsIgnoreCase("зеленый"))
        {
           red = 0;
           green = 128;
           blue = 0;
        }           
        
    }
    
    
    
    /**
     * @return the red
     */
    public int getRed()
    {
        return red;
    }

    /**
     * @param red the red to set
     */
    public void setRed(int red)
    {
        this.red = red;
    }

    /**
     * @return the green
     */
    public int getGreen()
    {
        return green;
    }

    /**
     * @param green the green to set
     */
    public void setGreen(int green)
    {
        this.green = green;
    }

    /**
     * @return the blue
     */
    public int getBlue()
    {
        return blue;
    }

    /**
     * @param blue the blue to set
     */
    public void setBlue(int blue)
    {
        this.blue = blue;
    }
  
   
    
}

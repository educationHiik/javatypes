/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatypes;

/**
 *
 * @author vaganovdv
 */
public class Point
{
    private double x;       // Координата x
    private double y;       // Координата y
    private double z;       // Координата z
    private Color color;    // Цвет точки
    private String name;    // Имя галактики

    public double getX()
    {
        return x;
    }

    public void setX(double x)
    {
        
        this.x = x;
    }

    public double getY()
    {
        return y;
    }

    public void setY(double y)
    {
        this.y = y;
    }

    public double getZ()
    {
        return z;
    }

    public void setZ(double z)
    {
        this.z = z;
    }

    public Color getColor()
    {
        return color;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
    
    public void print()
    {
        System.out.println("x="+x+"\n"+"y="+y+"\n"+color+'\n'+name);
    }
    
    
}

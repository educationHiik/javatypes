/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatypes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author vaganovdv
 */
public class JavaTypes
{

    
    public static void main(String[] args)
    {
        int num1 = 5;
        double num2 = 5.75;
        double sk = 5E-07;
        String  animal = "кролик";
        
        
        
        
        double result = num1+num2;
        String s = "125.5,2,3,4,5 вышел зайчик погулять";
        
        
        //System.out.println("num1=" + num1);
        //System.out.println("num2="+num2);
        //System.out.println("sum ="+result);
        
        /*
        Point point1 = new Point();
        point1.setName("Тумманость Андромеды");
        point1.setX(1);
        point1.setY(7.34);
        point1.setZ(10);
        
        Point point2 = new Point();
        point2.setName("Тумманость Андромеды");  
        point2.setX(123);
        point2.setY(13);
        point2.setZ(78);
        
          
        Point point3 = new Point();
        point3.setName("Просто туманность");
        point3.setX(1);
        point3.setY(7.34);
        point3.setZ(10);
        
            
        Point point4 = new Point();
        point4.setName("Созвездие Тельца");
        point4.setX(7878);
        point4.setY(98098);
        point4.setZ(18767);
        
        
        
        // Точка с указанием координат для поиска
        Point pointTofind = new Point();
        pointTofind.setX(1);
        pointTofind.setY(7.34);
        pointTofind.setZ(10);
        
        
         // Точка с указанием координат для поиска
        Point noPoint = new Point();
        noPoint.setX(10);
        noPoint.setY(10);
        noPoint.setZ(10);
        
        
        
        // Создание экз. класса
        PointList pointDatabase = new PointList();
        
        // Заполнение списка точками
        pointDatabase.getPoints().add(point2);
        pointDatabase.getPoints().add(point3);
        pointDatabase.getPoints().add(point1);
        pointDatabase.getPoints().add(point4);
        
        // Поиск точки в списке с указанными координатами
        
        
        
        
        System.out.println("Поиск точки с координатами "
                + "X = [" + pointTofind.getX() + "] "
                + "Y = [" + pointTofind.getY() + "] "
                + "Z = [" + pointTofind.getZ() + "] ... ");
        
        Optional<List<Point>> opt = pointDatabase.findPoint(pointTofind);
        //Optional<List<Point>> opt = pointDatabase.findPoint(noPoint);
        
        if (opt.isPresent())
        {
            List<Point> found = new ArrayList<>();
            // Размещение результатов поиска в списке found
            // (формирование списка из стороннего списка)
            found.addAll(opt.get());
            System.out.println("Поиск завершен успешно, найдено {"+found.size()+"} записей {Point}");
            
            for (int i = 0; i < found.size(); i++)
            {
                String galactika = found.get(i).getName();
                double X = found.get(i).getX();
                double Y = found.get(i).getY();
                double Z = found.get(i).getZ();
                
                String out = String.format("%-20s %-25s %-15s %-15s %-15s", "Точка найдена", "{"+galactika+"}", "X= ["+X+"]", "Y= ["+Y+"]", "Z= ["+Z+"]");
                
                System.out.println(out);
                
             
                System.out.println("Точка найдена {" + found.get(i).getName() + "} "
                        + "X = [" + X + "] "
                        + "Y = [" + Y + "] "
                        + "Z = [" + Z + "] ");
             
            }  
        }
        else
        {
             System.out.println("ПРЕДУПРЕЖДЕНИЕ: Отсутвуют в БД точки с координатами "
                + "X = [" + pointTofind.getX() + "] "
                + "Y = [" + pointTofind.getY() + "] "
                + "Z = [" + pointTofind.getZ() + "]");
        }
        
        
        
        
        
        int  k = pointDatabase.getPoints().size();
                
        
                
        
        
                            |  |  | 
        Color color1 = new Color(23,2,55);
        Color color2 = new Color(77,1,45);
        Color color3 = new Color(0,1,45);
        Color color4 = new Color("класный");
        Color color5 = new Color("зеленый");
        
        */
        
       // point1.print();
        
        //Circle circle = new Circle() ;
        
        Bank bank  = new Bank();
        
        // Cоздание экз.    передача Bank в класс   подписка на события
        //        /             /                     |                      
        B1 b1 = new B1(); b1.setBank(bank);         b1.init();
       // B2 b2 = new B2(); b2.setBank(bank);         b2.init();
        B3 b3 = new B3(); b3.setBank(bank);         b3.init();
        
        // Банковские операции
        bank.setOperation("Приход  100 р");
        bank.setOperation("Расход  20 р");
        bank.setOperation("Блокировка счета");
        
        
        
    }
    
}

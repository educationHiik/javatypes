/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatypes;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 *
 * @author vaganovdv
 */
public class B1
{

  
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    
    private Bank bank;
    
    
    
    public void init()
    {
        if (bank != null)
        {
            // Подписка на события класса Bank  обработчик события банка
            //                                    |
            bank.addPropertyChangeListener(BankListener);
        }    
    }        
    
     /**
     * Обработчик события
     */
    
       private final PropertyChangeListener BankListener = (PropertyChangeEvent evt)
            ->
    {
        if (evt != null)
        {
            //System.out.println("[B1] Получено событие от банка  ==> " + evt.getPropertyName());
            if (evt.getPropertyName().startsWith("Приход"))
            {
                System.out.println("[B1] Зачислено на счет ==> {"+evt.getNewValue()+"}");
            }   
        }

    };
    
    
    
    public Bank getBank()
    {
        return bank;
    }

    
    public void setBank(Bank bank)
    {
        this.bank = bank;
    }
    
    
    
}


package javatypes;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 *
 * @author vaganovdv
 */
public class Bank
{

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    
    
    // Банковская операция 
    private String operation = "Нет операции";

  
    /**
     * Получение сведений об операции
     * @return 
     */
    public String getOperation()
    {
        return operation;
    }

  
    /**
     * Установка свойств - изменение поля {operation}
     * 
     * @param operation 
     */
    public void setOperation(String operation)
    {
        System.out.println("[Bank] Поле {operation} изменено, новое значение ==> "+operation);
        if (operation.startsWith("Приход"))
        {
            // Инициализация события измнения свойств    поле PropertyName   старое значение      новое значение
            //                                               |               /                    /
            getPropertyChangeSupport().firePropertyChange("Приход",  this.operation,           operation);
        }  
        
        if (operation.startsWith("Расход"))
        {
            // Инициализация события измнения свойств 
            getPropertyChangeSupport().firePropertyChange("Расход",  this.operation, operation);
        }  
        
          
        if (operation.startsWith("Блокировка"))
        {
            // Инициализация события измнения свойств 
            getPropertyChangeSupport().firePropertyChange("Блокировка",  this.operation, operation);
        }  
        
        
            
        if (operation.startsWith("Арест"))
        {
            // Инициализация события измнения свойств 
            getPropertyChangeSupport().firePropertyChange("Арест",  this.operation, operation);
        }  
        
        
        
        this.operation = operation;
    }

  

    /**
     * Добавление слушателя 
     * 
     * @param listener 
     */
    public void addPropertyChangeListener(PropertyChangeListener listener)
    {
        System.out.println("[Bank] Добавлен слушатель "+listener.getClass().getSimpleName());
        getPropertyChangeSupport().addPropertyChangeListener(listener);
    }

   
    /**
     * Удаление слушаеля
     * 
     * @param listener 
     */
    public void removePropertyChangeListener(PropertyChangeListener listener)
    {
        getPropertyChangeSupport().removePropertyChangeListener(listener);
    }

    /**
     * @return the propertyChangeSupport
     */
    public PropertyChangeSupport getPropertyChangeSupport()
    {
        return propertyChangeSupport;
    }

    
    
}
